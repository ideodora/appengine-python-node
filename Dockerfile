# Pull base image
FROM node:4.8-onbuild
MAINTAINER ideodora <ideodora@gmail.com>

# Install appengine python sdk
ENV GAE_SDK_VERSION 1.9.60

# Dependencies we need for running phantomjs
ENV phantomJSDependencies\
    libicu-dev libfontconfig1-dev libjpeg-dev libfreetype6 openssl

# Dependencies for install pycrypto
ENV pycryptDependencies\
    build-essential libssl-dev libffi-dev python-dev
     
# Install system libs
RUN \
    apt-get update && \
    apt-get install -y python-pip && \
    apt-get install -y xvfb && \
    apt-get install -y --no-install-recommends unzip && \
    apt-get install -fyqq ${phantomJSDependencies} && \
    apt-get install -fyqq ${pycryptDependencies} && \
        # Hard copy phantomjs binary
    bash -c 'mkdir -p /phantomjs/phantomjs-2.1.1/bin'

COPY lib/phantomjs /phantomjs/phantomjs-2.1.1/bin/

RUN \
    # Make symlink for phantomjs
    ln -s /phantomjs/phantomjs-2.1.1/bin/phantomjs /usr/local/share/phantomjs && \
    ln -s /phantomjs/phantomjs-2.1.1/bin/phantomjs /usr/local/bin/phantomjs && \
    ln -s /phantomjs/phantomjs-2.1.1/bin/phantomjs /usr/bin/phantomjs

RUN \
    # Checking if phantomjs works
    phantomjs -v

# Install appengine libs
RUN \
    wget https://storage.googleapis.com/appengine-sdks/featured/google_appengine_${GAE_SDK_VERSION}.zip -P /tmp/ &&\
    mkdir -p /usr/local/google &&\
    unzip /tmp/google_appengine_${GAE_SDK_VERSION}.zip -d /usr/local/google/ &&\
    rm -rf /tmp/google_appengine_${GAE_SDK_VERSION}.zip

# Set path
ENV PATH /usr/local/google/google_appengine:${PATH}
ENV PYTHONPATH /usr/local/google/google_appengine:${PYTHONPATH}

# Define default command.
CMD ["bash"]
